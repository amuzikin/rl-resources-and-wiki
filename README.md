# RL RESOURCES AND WIKI

Collection of Reinforcement Learning Related links

##### Essentials:
##### 0. RL Bible, latest edition, pdf:
[`Reinforcement Learning: An Introduction` by Richard S. Sutton and Andrew G. Barto](http://incompleteideas.net/book/RLbook2020.pdf)

##### 1. RL Course by David Silver,
[slides](http://www0.cs.ucl.ac.uk/staff/d.silver/web/Teaching.html) and
[videos](https://www.youtube.com/watch?v=2pWv7GOvuf0);

##### 2. Berkeley Deep RL course CS 294-112, 
lecture slides, videos and literature links:
[Current Year](http://rail.eecs.berkeley.edu/deeprlcourse-fa18/)

##### 3. Excellent lecture by prof. Sutton on TD-learning:
<a href='http://videolectures.net/deeplearning2017_sutton_td_learning/'>
  <img src='http://videolectures.net/deeplearning2017_sutton_td_learning/thumb.jpg' border=0/>
  <br/>TD Learning</a><br/>
Richard S. Sutton

##### 4. Practical Deep RL course by YandexDataShchool: 
[github_repository](https://github.com/yandexdataschool/Practical_RL)

*****

##### For extensive resources lists follow these extensive collections:

https://github.com/muupan/deep-reinforcement-learning-papers

https://github.com/songrotek/Meta-Learning-Papers

https://github.com/LantaoYu/MARL-Papers

https://github.com/endymecy/awesome-deeplearning-resources

https://github.com/dbobrenko/reinforcement-learning-notes

https://github.com/camigord/DRL_papernotes

https://github.com/camigord/DRL_papernotes/tree/master/notes/hierarchical-learning

https://github.com/L706077/Deep-Reinforcement-Learning-Papers

*****

##### 25.11.2021: This page has been cloned from [source](https://github.com/Kismuz/btgym/wiki/RL-WATCH-&-READ-shortlist)






